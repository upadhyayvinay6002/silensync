import React from 'react';
import { SafeAreaView, StatusBar, StyleSheet, View, useColorScheme } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useTheme } from '@react-navigation/native';

interface Props {
    children: any,
}

const CustomBG: React.FC<Props> = ({ children }) => {
    const {colors} = useTheme();
    const themeColor = useColorScheme();
    return (
        <LinearGradient
            style={style.container}
            colors={['#191919', '#cccccc']}
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 0 }}
        >
            <LinearGradient style={{ width: '5%', height: 80, alignSelf: 'flex-start', position: 'absolute', borderBottomRightRadius: 100, zIndex: 0 }} colors={['#F8C8DC', '#FF00FF']} start={{ x: 0, y: 0 }} />
            <LinearGradient style={{ width: '5%', height: 60, alignSelf: 'flex-start', position: 'absolute', borderTopRightRadius: 100, zIndex: 0, left: '9.5%', top: -20, transform: [{ rotate: '90deg' }] }} start={{ x: 0, y: 0 }} colors={['#FF00FF', '#F8C8DC']} />
            <LinearGradient style={{
                width: '5%', height: 60, alignSelf: 'flex-end', position: 'absolute', borderBottomRightRadius: 100,
                right: '9.5%', top: -20, transform: [{ rotate: '90deg' }]
            }} start={{ x: 0, y: 0 }} colors={['#F8C8DC', '#FF00FF']} />
            <LinearGradient style={{
                width: '5%', height: 80, alignSelf: 'flex-end', position: 'absolute', borderBottomLeftRadius: 100,
                right: 0, top: 0
            }} start={{ x: 0, y: 0 }} colors={['#F8C8DC', '#FF00FF']} />
            <StatusBar barStyle={themeColor ? 'light-content' : 'dark-content'} />
            <LinearGradient style={{ position: 'absolute', width: '50%', height: '25%', alignSelf: 'flex-end', top: '15%', borderRadius: 100, left: '76%' }}
                colors={['#EDF1F4', '#C3CBDC']} start={{ x: 1, y: 1 }} end={{ x: 0, y: 0 }} />
            <LinearGradient style={{ position: 'absolute', width: '43%', height: '22%', alignSelf: 'flex-end', top: '16.5%', borderRadius: 100, left: '79%', zIndex: 30 }}
                colors={['#ffcccc', '#ff6666']} start={{ x: 1, y: 1 }} end={{ x: 0, y: 0 }} />
            <LinearGradient style={{ position: 'absolute', width: '37%', height: '19%', alignSelf: 'flex-end', top: '18%', borderRadius: 100, left: '82%', zIndex: 30 }}
                colors={['#00F3FF', '#279FA5']} start={{ x: 1, y: 1 }} end={{ x: 0, y: 0 }} />
            <LinearGradient style={{ position: 'absolute', width: '32%', height: '16.5%', alignSelf: 'flex-end', top: '19%', borderRadius: 100, left: '84%', zIndex: 30 }}
                colors={['#F687E0', '#E75CCC']} start={{ x: 1, y: 1 }} end={{ x: 0, y: 0 }} />
            <LinearGradient style={{ margin: '0%', zIndex: 999, flex: 1, borderRadius: 0, opacity: 0.8, padding: '2%' }}
                colors={colors.bgColor} start={{ x: 1, y: 1}} end={{ x: 1, y: 0 }} >
                {children}
            </LinearGradient>
            <LinearGradient style={{ position: 'absolute', width: '50%', height: '25%', alignSelf: 'flex-start', top: '60%', borderRadius: 100, right: '75%' }}
                colors={['#C3CBDC', '#EDF1F4']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }} />
            <LinearGradient style={{ position: 'absolute', width: '43%', height: '22%', alignSelf: 'flex-end', top: '60%', borderRadius: 100, right: '79%', zIndex: 30 }}
                colors={['#ffcccc', '#ff6666']} start={{ x: 1, y: 1 }} end={{ x: 0, y: 0 }} />
            <LinearGradient style={{ position: 'absolute', width: '37%', height: '19%', alignSelf: 'flex-end', top: '60%', borderRadius: 100, right: '82%', zIndex: 30 }}
                colors={['#00F3FF', '#279FA5']} start={{ x: 1, y: 1 }} end={{ x: 0, y: 0 }} />
            <LinearGradient style={{ position: 'absolute', width: '32%', height: '16.5%', alignSelf: 'flex-end', top: '60%', borderRadius: 100, right: '84%', zIndex: 30 }}
                colors={['#F687E0', '#E75CCC']} start={{ x: 1, y: 1 }} end={{ x: 0, y: 0 }} />
            <LinearGradient style={{
                width: '5%', height: 60, alignSelf: 'flex-end', position: 'absolute', borderBottomLeftRadius: 100,
                right: '9.5%', bottom: -20, transform: [{ rotate: '90deg' }]
            }} start={{ x: 0, y: 0 }} colors={['#F8C8DC', '#FF00FF']} />
            <LinearGradient style={{
                width: '5%', height: 80, alignSelf: 'flex-end', position: 'absolute', borderTopLeftRadius: 100,
                right: 0, bottom: 0
            }} start={{ x: 0, y: 0 }} colors={['#FF00FF', '#F8C8DC']} />

            <LinearGradient style={{
                width: '5%', height: 60, alignSelf: 'flex-end', position: 'absolute', borderTopLeftRadius: 100,
                left: '9.5%', bottom: -20, transform: [{ rotate: '90deg' }]
            }} start={{ x: 0, y: 0 }} colors={['#FF00FF', '#F8C8DC']} />
            <LinearGradient style={{
                width: '5%', height: 80, alignSelf: 'flex-end', position: 'absolute', borderTopRightRadius: 100,
                left: 0, bottom: 0
            }} start={{ x: 0, y: 0 }} colors={['#FF00FF', '#F8C8DC']} />

        </LinearGradient >
    );
};

const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
});

export default CustomBG;

import { Text, TouchableOpacity } from "react-native";
import { fontSize, hp } from "../../utils/LayoutDiamentions";
import { useTheme } from "@react-navigation/native";
import LinearGradient from "react-native-linear-gradient";

interface Props {
    title: string,
    onPress: () => void;
    style: object,
    disable: boolean,
}

const CustomButtom: React.FC<Props> = ({ title, onPress, style, disable=false }) => {
    const Theme = useTheme();
    const { colors } = Theme;

    return (
        <LinearGradient colors={!disable ? colors.buttomColor : ['#EDEADE', '#EDEADE']} style={[{ width: '100%', borderRadius:20 }, style]}>
            <TouchableOpacity onPress={onPress} style={{ width: '100%', height: hp(7), justifyContent: 'center', alignItems: 'center' }}
                disabled={disable}
            >
                <Text style={{ fontSize: fontSize(6), fontFamily: 'Cormorant-Bold', color: !disable ? colors.textColor : '#36454F' }}>
                    {title}
                </Text>
            </TouchableOpacity>
        </LinearGradient>
    )
}

export default CustomButtom;
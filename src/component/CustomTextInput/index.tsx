import React from 'react';
import { Image, StyleSheet, TextInput, View, TextInputProps, TouchableOpacity } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { fontSize, hp } from '../../utils/LayoutDiamentions';
import { DKImages } from '../../assets/images';

interface Props extends TextInputProps {
    placeholder: string;
    onChangeText: (text: string) => void;
    style?: any;
    isSecure: boolean;
    icons?: any;
    isPassword?: boolean;
    placeholderTextColor?: string;
    onChangeSecure: () => void;
}

const CustomTextInput: React.FC<Props> = ({
    placeholder,
    onChangeText,
    style,
    isSecure,
    autoCapitalize = 'none',
    autoComplete = 'off',
    autoCorrect = false,
    cursorColor = 'black',
    editable = true,
    icons = null,
    placeholderTextColor,
    isPassword = false,
    onChangeSecure,
    ...rest
}) => {
    const { colors } = useTheme();

    return (
        <View style={[styles.container, { backgroundColor: colors.textInputBackgroundColor },  style]}>
            <TextInput
                placeholder={placeholder}
                onChangeText={onChangeText}
                style={[
                    styles.textInputContainer,
                    { color: colors.textColor, width: isPassword ? '90%' : '100%' },
                ]}
                secureTextEntry={isSecure}
                autoCapitalize={autoCapitalize}
                autoComplete={autoComplete}
                autoCorrect={autoCorrect}
                cursorColor={cursorColor}
                editable={editable}
                placeholderTextColor={placeholderTextColor || colors?.greyText}
                {...rest}
            />
            {icons ? (
                <Image source={icons} style={styles.icon} />
            ) : isPassword ? (
                <TouchableOpacity style={styles.iconContainer} onPress={onChangeSecure}>
                    <Image source={isSecure ? DKImages.Hide : DKImages.Show} style={styles.icon} />
                </TouchableOpacity>
            ) : null}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        borderRadius: 15,
        marginVertical: hp(1),
        paddingHorizontal: '2%',
        width: '100%',
        paddingVertical: hp(0.2),
        flexDirection: 'row',
        alignItems: 'center',
    },
    textInputContainer: {
        flex: 1,
        fontFamily: 'Cormorant-Medium',
        fontSize: fontSize(4),
    },
    icon: {
        width: hp(2.5),
        height: hp(2.5),
        tintColor: '#B3B3B3',
    },
    iconContainer: {
        width: '10%',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default CustomTextInput;

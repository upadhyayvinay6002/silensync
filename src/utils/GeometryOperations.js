import Geolocation from '@react-native-community/geolocation';
export const calculateDistance = (coord1, coord2) => {
  const deg2rad = degrees => {
    return degrees * (Math.PI / 180);
  };
  console.log(coord1, coord2);
  const earthRadius = 6371;
  const lat1 = deg2rad(coord1.latitude);
  const lon1 = deg2rad(coord1.longitude);
  const lat2 = deg2rad(coord2.latitude);
  const lon2 = deg2rad(coord2.longitude);

  // Haversine formula
  const dLat = lat2 - lat1;
  const dLon = lon2 - lon1;
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const distance = earthRadius * c;
  return distance;
};

import {PermissionsAndroid, Alert, BackHandler} from 'react-native';
export const checkLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message:
            'App needs access to your location for accessing location and mute.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
        showPermissionDeniedAlert();
      }
    } catch (err) {
      showPermissionDeniedAlert();
    }
  };

  const showPermissionDeniedAlert = () => {
    Alert.alert(
      'Permission Required',
      'Without appropriate permission, we cannot proceed.',
      [{text: 'OK', onPress: () => exitApp()}],
      {cancelable: false},
    );
  };

  const exitApp = () => {
    BackHandler.exitApp();
  };
import { useColorScheme } from 'react-native';

export const theme = () => {
  const colors = useColorScheme();

  const darkTheme = {
    colors: {
        bgColor: ['#000000', '#191919'],
        textColor: '#fff',
        secondaryText: '#D1D3E6',
        greyText: '#B3B3B3',
        textInputBackgroundColor: '#4c4c4c',
        buttomColor: ['#318CE7', '#0039a6']
    },
  };
  
  const lightTheme = {
    colors: {
        bgColor: ['#B3B3B3', '#E0E0E0'],
    },
  };

  return colors === 'dark' ? darkTheme : lightTheme;
};

import React from 'react';
import {View, Button} from 'react-native';

const Home = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'cyan',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Button
        title="Select Location"
        onPress={() => navigation.navigate('MapMark')}
      />
    </View>
  );
};

export default Home;

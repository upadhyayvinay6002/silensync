import React, { useState, useEffect } from 'react';
import { View, Text, Button, ActivityIndicator, NativeModules, ToastAndroid } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import { calculateDistance } from '../../utils/GeometryOperations';

const MapMark = () => {
  const [selectedLocation, setSelectedLocation] = useState(null);
  const [userCurrentLocation, setUserCurrentLocation] = useState({});
  const [loading, setLoading] = useState(true);
  const { AudioManagerModule } = NativeModules;

  useEffect(() => {
    fetchCurrentLocation();
  }, []);

  useEffect(() => {
    const watchId = Geolocation.watchPosition(
      position => {
        const { latitude, longitude } = position.coords;
        setUserCurrentLocation({ latitude, longitude });
      },
      error => {
        ToastAndroid.show(error.message, ToastAndroid.SHORT);
      },
      {
        enableHighAccuracy: true,
        distanceFilter: 10,
        interval: 10000,
        fastestInterval: 5000,
        useSignificantChanges: true,
        showsBackgroundLocationIndicator: true,
      },
    );

    return () => {
      Geolocation.clearWatch(watchId);
    };
  }, []);

  const fetchCurrentLocation = async () => {
    try {
      const position = await getCurrentLocation();
      setUserCurrentLocation(position);
      setLoading(false);
    } catch (error) {
      console.error('Error getting location:', error);
      setLoading(false);
    }
  };

  const getCurrentLocation = () => {
    return new Promise((resolve, reject) => {
      Geolocation.getCurrentPosition(
        position => {
          const { latitude, longitude } = position.coords;
          resolve({ latitude, longitude });
        },
        error => {
          reject(error);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
      );
    });
  };

  const handleLocationSelect = event => {
    const selectedCoordinates = event.nativeEvent.coordinate;
    setSelectedLocation(selectedCoordinates);
  };

  const muteDeviceWhenNearLocation = async () => {
    if (!selectedLocation) {
      return;
    }
    const isNearLocation =
      calculateDistance(userCurrentLocation, selectedLocation) < 0.05;
    if (isNearLocation) {
      AudioManagerModule.muteCall();
      AudioManagerModule.showToast('Device Muted successfully');
    } else {
      AudioManagerModule.unmuteCall();
      AudioManagerModule.showToast('Device Unmuted successfully');
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <Text style={{ fontSize: 22, textAlign: 'center' }}>Select a Location</Text>
      {loading ? (
        <ActivityIndicator size="large" color="#0000ff" />
      ) : (
        <>
          <MapView
            style={{ flex: 1 }}
            initialRegion={{
              latitude: userCurrentLocation?.latitude,
              longitude: userCurrentLocation?.longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            zoomControlEnabled={true}
            zoomEnabled={true}
            onPress={handleLocationSelect}
          >
            {userCurrentLocation && (
              <Marker coordinate={userCurrentLocation} title="Your Location" />
            )}
            {selectedLocation && (
              <Marker coordinate={selectedLocation} title="Selected Location" />
            )}
          </MapView>
          <Button
            title="Mute When Near"
            onPress={() => muteDeviceWhenNearLocation()}
          />
        </>
      )}
    </View>
  );
};

export default MapMark;

import {
    View,
    Text,
    KeyboardAvoidingView,
    Image,
    TouchableOpacity,
  } from 'react-native';
  import React, {useEffect, useState} from 'react';
  import {CustomBG, CustomTI, CustomButtom} from '../../component';
  import {useTheme} from '@react-navigation/native';
  import {hp, wp, fontSize} from '../../utils/LayoutDiamentions';
  import {DKImages} from '../../assets/images';
  import {GoogleSignin} from '@react-native-google-signin/google-signin';
  import auth from '@react-native-firebase/auth';
  
  const Signup = ({navigation}) => {
    const Theme = useTheme();
    const {colors} = Theme;
  
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [userDetail, setUserDetail] = useState('');
    const [isSecuire, setIsSecuire] = useState(true);
  
    useEffect(() => {
      GoogleSignin.configure();
    });
  
    const googleSignin = async () => {
      try {
        await GoogleSignin.hasPlayServices();
        const userInfo = await GoogleSignin.signIn();
        if(userInfo){
          setUserDetail(userInfo);
          const { accessToken } = await GoogleSignin.getTokens();
          const googleCredential = auth.GoogleAuthProvider.credential(null, accessToken);
          await auth().signInWithCredential(googleCredential);
        }
      } catch (error) {
        console.log('error==============', error);
      }
    };
  
    console.log('userDetail', userDetail);
  
    return (
      <CustomBG>
        <View style={{flex: 1, paddingHorizontal: '4%'}}>
          <Image
            source={DKImages.Logo}
            style={{width: hp(20), height: hp(20), marginTop: hp(3)}}
            resizeMode="contain"
          />
          <Text
            style={{
              marginTop: hp(3),
              fontSize: fontSize(12),
              lineHeight: hp(10),
              color: colors.textColor,
              fontFamily: 'Cormorant-Bold',
            }}>
            Join us for free
          </Text>
          <Text
            style={{
              fontSize: fontSize(7),
              lineHeight: hp(4),
              color: colors.secondaryText,
              fontFamily: 'Cormorant-Medium',
            }}>
            Selected place
          </Text>
          <Text
            style={{
              fontSize: fontSize(7),
              color: colors.secondaryText,
              fontFamily: 'Cormorant-Medium',
            }}>
            your device will be silence.
          </Text>
          <KeyboardAvoidingView
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
            behavior={'height'}
            keyboardVerticalOffset={hp(10)}>
            <CustomTI
              placeholder="Email"
              style={{marginTop: hp(3), marginBottom: hp(1)}}
              onChangeText={usernameInput => setUsername(usernameInput)}
            />
            <CustomTI
              placeholder="Password"
              onChangeText={passwordInput => setPassword(passwordInput)}
              isPassword
              isSecure={isSecuire}
              onChangeSecure={() => setIsSecuire(prevState => !prevState)}
            />
            <CustomTI
              placeholder="Re-Password"
              onChangeText={passwordInput => setPassword(passwordInput)}
            />
          </KeyboardAvoidingView>
          <CustomButtom
            title="Sign up"
            style={{marginBottom: hp(14), top: hp(5)}}
          />
          <TouchableOpacity
            style={{
              width: '100%',
              borderRadius: 20,
              height: hp(6.5),
              bottom: hp(7),
              backgroundColor: colors.textColor,
              justifyContent: 'space-between',
              flexDirection: 'row',
              paddingHorizontal: '21%',
            }}
            onPress={googleSignin}
            activeOpacity={0.5}>
            <Image
              source={DKImages.Google}
              style={{width: hp(5), height: hp(5), alignSelf: 'center'}}
              resizeMode="contain"
            />
            <Text
              style={{
                fontSize: fontSize(6),
                color: '#000',
                fontFamily: 'Cormorant-Medium',
                alignSelf: 'center',
              }}>
              {' '}
              Sign in with Google
            </Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              bottom: hp(6),
              justifyContent: 'center',
              alignItems: 'center',
              paddingHorizontal: '3%',
            }}>
            <View style={{flexDirection: 'row', gap: hp(1)}}>
              <Text
                style={{
                  fontSize: fontSize(5),
                  color: colors.secondaryText,
                  fontFamily: 'Cormorant-Medium',
                  color: colors?.greyText,
                }}>
                Already have an account? 
              </Text>
              <TouchableOpacity activeOpacity={0.5} onPress={() => navigation.navigate('login')}>
                <Text
                  style={{
                    fontSize: fontSize(5),
                    color: colors.secondaryText,
                    fontFamily: 'Cormorant-Medium',
                    color: '#6CB4EE',
                  }}>
                  Login
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </CustomBG>
    );
  };
  
  export default Signup;
  
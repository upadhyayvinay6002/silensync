import {
  View,
  Text,
  KeyboardAvoidingView,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {CustomBG, CustomTI, CustomButtom} from '../../component';
import {useTheme} from '@react-navigation/native';
import {hp, wp, fontSize} from '../../utils/LayoutDiamentions';
import {DKImages} from '../../assets/images';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';

const Login = ({navigation}) => {
  const Theme = useTheme();
  const {colors} = Theme;

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [userDetail, setUserDetail] = useState('');
  const [isSecuire, setIsSecuire] = useState(true);

  useEffect(() => {
    GoogleSignin.configure();
  });

  const googleSignin = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      if(userInfo){
        setUserDetail(userInfo);
        const { accessToken } = await GoogleSignin.getTokens();
        const googleCredential = auth.GoogleAuthProvider.credential(null, accessToken);
        await auth().signInWithCredential(googleCredential);
      }
    } catch (error) {
      console.log('error==============', error);
    }
  };


  return (
    <CustomBG>
      <View style={{flex: 1, paddingHorizontal: '4%'}}>
        <Image
          source={DKImages.Logo}
          style={{width: hp(20), height: hp(20), marginTop: hp(3)}}
          resizeMode="contain"
        />
        <Text
          style={{
            marginTop: hp(3),
            fontSize: fontSize(12),
            lineHeight: hp(10),
            color: colors.textColor,
            fontFamily: 'Cormorant-Bold',
          }}>
          Let's sign you in
        </Text>
        <Text
          style={{
            fontSize: fontSize(7),
            lineHeight: hp(4),
            color: colors.secondaryText,
            fontFamily: 'Cormorant-Medium',
          }}>
          Welcome back.
        </Text>
        <Text
          style={{
            fontSize: fontSize(7),
            color: colors.secondaryText,
            fontFamily: 'Cormorant-Medium',
          }}>
          you have been missed!
        </Text>
        <KeyboardAvoidingView
          style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
          behavior={'height'}
          keyboardVerticalOffset={hp(8)}>
          <CustomTI
            placeholder="Email"
            style={{marginTop: hp(5), marginBottom: hp(1)}}
            onChangeText={usernameInput => setUsername(usernameInput)}
          />
          <CustomTI
            placeholder="Password"
            onChangeText={passwordInput => setPassword(passwordInput)}
            isPassword
            isSecure={isSecuire}
            onChangeSecure={() => setIsSecuire(prevState => !prevState)}
          />
        </KeyboardAvoidingView>
        <CustomButtom
          title="Login"
          style={{marginBottom: hp(14), top: hp(4)}}
        />
        <TouchableOpacity
          style={{
            width: '100%',
            borderRadius: 20,
            height: hp(6.5),
            bottom: hp(8),
            backgroundColor: colors.textColor,
            justifyContent: 'space-between',
            flexDirection: 'row',
            paddingHorizontal: '21%',
          }}
          onPress={googleSignin}
          activeOpacity={0.5}>
          <Image
            source={DKImages.Google}
            style={{width: hp(5), height: hp(5), alignSelf: 'center'}}
            resizeMode="contain"
          />
          <Text
            style={{
              fontSize: fontSize(6),
              color: '#000',
              fontFamily: 'Cormorant-Medium',
              alignSelf: 'center',
            }}>
            {' '}
            Sign in with Google
          </Text>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            bottom: hp(6),
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: '3%',
          }}>
          <TouchableOpacity activeOpacity={0.5}>
            <Text
              style={{
                fontSize: fontSize(5),
                color: colors.secondaryText,
                fontFamily: 'Cormorant-Medium',
                color: '#6CB4EE',
              }}>
              Forgot Password
            </Text>
          </TouchableOpacity>
          <View style={{flexDirection: 'row', gap: hp(1)}}>
            <Text
              style={{
                fontSize: fontSize(5),
                color: colors.secondaryText,
                fontFamily: 'Cormorant-Medium',
                color: colors?.greyText,
              }}>
              * Don't have an account?
            </Text>
            <TouchableOpacity activeOpacity={0.5} onPress={()=> navigation.navigate('signup')}>
              <Text
                style={{
                  fontSize: fontSize(5),
                  color: colors.secondaryText,
                  fontFamily: 'Cormorant-Medium',
                  color: '#6CB4EE',
                }}>
                Sign up
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </CustomBG>
  );
};

export default Login;

import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import Onboarding from 'react-native-onboarding-swiper';
import LottieView from 'lottie-react-native';
import {useNavigation} from '@react-navigation/native';
import {wp, hp, fontSize} from '../../utils/LayoutDiamentions';
import {Animation} from '../../assets/index';

const Landing = () => {
  const navigation = useNavigation();

  const handleDoneClick = () => {
    navigation.navigate('Login');
  };

  const onButtonComponent = ({...props}) => {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: '#E3DAC9',
          width: '55%',
          height: '85%',
          flex:1,
          borderBottomLeftRadius: 25,
          borderTopLeftRadius: 25,
          justifyContent:'center',
          alignItems:'center',
          marginBottom: '3%'
        }}>
          <Image
        source={{ uri: 'https://media.giphy.com/media/UX5ovY9QQ1FOpaKtc8/giphy.gif' }}
        style={{ width: '100%', height: '100%', borderRadius: 90 }}
      />
        {/* <Text style={{color: 'black', fontSize: 12, fontWeight: 'bold'}}>Done</Text> */}
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Onboarding
        onDone={handleDoneClick}
        onSkip={handleDoneClick}
        bottomBarHighlight={false}
        DoneButtonComponent={onButtonComponent}
        containerStyle={{width: '100%', paddingHorizontal: '2%'}}
        pages={[
          {
            backgroundColor: '#845943',
            image: (
              <View style={{width: '95%', height: '70%'}}>
                <LottieView source={Animation.buy} autoPlay loop />
              </View>
            ),
            title: (
              <Text style={{fontSize: fontSize(8), color: '#FFDAB9'}}>
                Forgot to mute
              </Text>
            ),
            subtitle: (
              <Text style={{fontSize: fontSize(6), color: '#FFDAB9'}}>
                Getting late for office
              </Text>
            ),
          },
          {
            backgroundColor: '#ac7c60',
            image: (
              <View style={{width: '95%', height: '70%'}}>
                <LottieView source={Animation.manWaiv} autoPlay loop />
              </View>
            ),
            title: (
              <Text
                style={{
                  fontSize: fontSize(8),
                  color: '#543D21',
                  textAlign: 'center',
                }}>
                Thinks about to mute
              </Text>
            ),
            subtitle: (
              <Text style={{fontSize: fontSize(6), color: '#543D21'}}>
                Want to mute the sound
              </Text>
            ),
          },
          {
            backgroundColor: '#d1a684',
            image: (
              <View style={{width: '95%', height: '70%'}}>
                <LottieView source={Animation.bell} autoPlay loop />
              </View>
            ),
            title: (
              <Text style={{fontSize: fontSize(8), color: '#2F3E46'}}>
                No Worry
              </Text>
            ),
            subtitle: (
              <Text style={{fontSize: fontSize(6), color: '#2F3E46'}}>
                Select location and done
              </Text>
            ),
          },
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Landing;

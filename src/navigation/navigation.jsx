import React, {useEffect} from 'react';
import {NavigationContainer, useNavigation, useTheme} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

/* Importing the screens */
import LandingPage from '../screens/Landing';
import Home from '../screens/Home';
import MapMark from '../screens/MapMark';
import Landing from '../screens/Landing';
import { checkLocationPermission } from '../utils/locationPermission';
import Login from '../screens/Login';
import { theme } from '../utils/ThemeBasedColor';
import Signup from '../screens/Signup';

/* Create a Stack variable */
const Stack = createNativeStackNavigator();

const Navigation = () => {
  const Theme = theme();

  useEffect(() => {
    checkLocationPermission();
  }, []);

  return (
    <NavigationContainer theme={Theme}>
      <Stack.Navigator initialRouteName="LandingPage" screenOptions={{headerShown:false}}>
      <Stack.Screen
         name='login'
         component={Login}
        />
        <Stack.Screen
         name='signup'
         component={Signup}
        />
        <Stack.Screen
         name='landing'
         component={Landing}
        />
        <Stack.Screen
          name="Home"
          component={Home}
        />
        <Stack.Screen
          name="MapMark"
          component={MapMark}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;

const Animation = {
    manWaiv : require('./Animations/lottie/man-waiving-hand.json'),
    bell: require('./Animations/lottie/BellOf.json'),
    buy: require('./Animations/lottie/buy-online.json')
}

// const Fonts = {
//     sound: require('../assets/fonts/Sound.ttf')
// }

export {Animation}
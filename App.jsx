import React from 'react';
import HomeNavigation from './src/navigation/Navigation';

const App = () => {
  return <HomeNavigation />;
};

export default App;

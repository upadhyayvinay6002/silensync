/**
 * @format
 */
import App from './App';
import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';

import { LogBox } from 'react-native';

// Ignore specific logs or all logs based on your preference
LogBox.ignoreLogs(['Warning: ...']); // Ignore log notifications by message
// LogBox.ignoreAllLogs(); // Ignore all log notifications

import Reactotron from 'reactotron-react-native'; // Import Reactotron
import {reactotronRedux} from 'reactotron-redux'; // Import reactotron-redux

// Always configure Reactotron, even in development mode
Reactotron.configure({name: 'MyApp'})
  .useReactNative()
  .use(reactotronRedux())
  .connect(); 

if (__DEV__) {
  // If you want to connect Reactotron only in development mode
  console.log('Reactotron Configured');
}

AppRegistry.registerComponent(appName, () => App);

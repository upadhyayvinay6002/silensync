package com.silensync;

import android.media.AudioManager;
import android.util.Log;
import android.widget.Toast;
import android.content.Context;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class AudioManagerModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private AudioManager audioManager;

    public AudioManagerModule(ReactApplicationContext context) {
        super(context);
        this.reactContext = context;
        this.audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    public String getName() {
        return "AudioManagerModule";
    }

    @ReactMethod
    public void muteCall() {
        if (audioManager != null) {
            audioManager.setStreamMute(AudioManager.STREAM_RING, true);
        }
    }

    @ReactMethod
    public void unmuteCall() {
        if (audioManager != null) {
            audioManager.setStreamMute(AudioManager.STREAM_RING, false);
        }
    }

    @ReactMethod
    public void showToast(String message) {
        Toast.makeText(getReactApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}

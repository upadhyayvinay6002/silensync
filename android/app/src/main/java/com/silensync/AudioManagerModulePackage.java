package com.silensync;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.silensync.AudioManagerModule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AudioManagerModulePackage implements ReactPackage {

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();
        modules.add(new AudioManagerModule(reactContext));
        return modules;
    }

    // If you have view managers, you can implement createViewManagers method as well.
    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }
}
